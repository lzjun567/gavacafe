###软件清单
项目运行在virtualenv下面  

    $ virtualenv ~/envs/gongshare --no-site-packages
    $ cd ~/envs/gongshare/bin
    $ source activate
    (gongshare)$ pip install django
    (gongshare)$ cd ~/gongshare

    pip install django-tinymce
    pip install south  处理数据库的
    pip install PIL    图像处理，上传文件的时候可以用
    pip install django-simple-captcha 验证码

    sudo apt-get install python-dev libmysqlclient-dev 注：mysql-python要依赖这两个包
    pip install MySQL-python
    pip install --upgrade distribute   如果提示distribute版本低，执行就执行该命令
    pip install publicsuffix 

错误解决：  
ValueError: [u'path'] [http://sourceforge.net/projects/pywin32/files/pywin32/Build%20214/pywin32-214.win-amd64-py2.7.exe/download]
Unable to find vcvarsall.bat[http://download.microsoft.com/download/A/5/4/A54BADB6-9C3F-478D-8657-93B3FC9FE62D/vcsetup.exe]
TypeError: function takes at most 4 arguments (6 given)[http://stackoverflow.com/questions/16365462/when-i-run-django-simple-captcha-test-throw-2-errors]

###数据库两个增量表创建

创建增量索引的时候用  

    CREATE DATABASE IF NOT EXISTS gongshare default charset utf8 COLLATE utf8_general_ci;
    CREATE TABLE `sph_tag_counter`(
        `counter_id` INT(11) NOT NULL,
        `max_doc_id` INT(11) NOT NULL,
        PRIMARY KEY(`counter_id`)
    )
    CREATE TABLE `sph_subject_counter`(
        `counter_id` INT(11) NOT NULL,
        `max_doc_id` INT(11) NOT NULL,
        PRIMARY KEY(`counter_id`)
    )

###全文检索引擎coorseek安装配置

1. 下载地址：[coreseek4.1](http://www.coreseek.cn/uploads/csft/4.0/coreseek-4.1-beta.tar.gz)
2. 预安装：

        apt-get install make gcc g++ automake libtool mysql-client libmysqlclient15-dev   libxml2-dev libexpat1-dev

3. 安装mmseg分词库

        tar -zvxf coreseek
        cd mmseg
        ./bootstrap
        ./configure --prefix=/usr/local/mmseg3
        make
        make install

4. 安装coreseek 

        cd csft
        sh buildconf.sh
        ./configure --prefix=/usr/local/coreseek  --without-unixodbc --with-mmseg --with-mmseg-includes=/usr/local/mmseg3/include/mmseg/ --with-mmseg-libs=/usr/local/mmseg3/lib/ --with-mysql
        make
        make install

5. 配置
[参考](http://www.coreseek.cn/products/products-install/coreseek_mmseg/)  
[LibMMSeg](http://www.coreseek.cn/opensource/mmseg/#mmseg_ini)   
对特殊短语的支持

    mmseg -b exceptions.txt
[exception.txt](conf/exception.txt)，生成的文件synonyms.dat放入uni.lib同级目录

    mv synonyms.dat /usr/local/mmseg/etc

###数据库迁移
对已经有数据表结构进行更新是可以使用south来操作，具体步骤：  

1. Make sure your django tables match your current database tables exactly - if you planned to add or remove columns, comment those out.
2. Run python manage.py schemamigration myapp --initial
3. Run python manage.py migrate myapp --fake
4. Make changes to your django model
5. Run python manage.py schemamigration myapp --auto
6. Run python manage.py migrate myapp

###压缩合并JS、CSS
合并js和css文件，减少HTTP请求  

    java -jar ..\..\dist\compiler.jar --language_in ECMASCRIPT5 --js jquery-1.8.2.min.js jquery.autocomplete.js gavacafe.js jquery.tipsy.js zebra_dialog.src.js --js_output_file application.js

    type autocomplete.css content.css gavacafe.css tipsy.css zebra_dialog.css > application_temp.css
    java -jar ..\..\dist\yuicompressor-2.4.7.jar application_temp.css > application.css

注释文件  

       <!--
       <script type="text/javascript" src="/site_media/jquery-1.8.2.min.js"></script>
       <script src="/site_media/jquery.autocomplete.js"></script>
       <script src="/site_media/gavacafe.js"></script>
	   <script src="/site_media/zebra_dialog.src.js"></script>
	   <script src="/site_media/jquery.tipsy.js"></script>
       -->

###其他笔记  
修改ubuntu时区：

    dpkg-reconfigure tzdata


