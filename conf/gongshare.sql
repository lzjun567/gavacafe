-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.10 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 gongshare 的数据库结构
CREATE DATABASE IF NOT EXISTS `gongshare` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gongshare`;


-- 导出  表 gongshare.account_myuser 结构
CREATE TABLE IF NOT EXISTS `account_myuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `weibo_uid` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `auth_token` varchar(100) NOT NULL,
  `expired_time` double DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `email` varchar(75),
  `add_time` datetime NOT NULL,
  `signature` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `active_key` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.account_myuser_groups 结构
CREATE TABLE IF NOT EXISTS `account_myuser_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `myuser_id` (`myuser_id`,`group_id`),
  KEY `account_myuser_groups_f1d9e869` (`myuser_id`),
  KEY `account_myuser_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_f508e480` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `myuser_id_refs_id_3bca27d5` FOREIGN KEY (`myuser_id`) REFERENCES `account_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.account_myuser_user_permissions 结构
CREATE TABLE IF NOT EXISTS `account_myuser_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `myuser_id` (`myuser_id`,`permission_id`),
  KEY `account_myuser_user_permissions_f1d9e869` (`myuser_id`),
  KEY `account_myuser_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `myuser_id_refs_id_62ed4079` FOREIGN KEY (`myuser_id`) REFERENCES `account_myuser` (`id`),
  CONSTRAINT `permission_id_refs_id_6c32d930` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.auth_group 结构
CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.auth_group_permissions 结构
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.auth_permission 结构
CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.captcha_captchastore 结构
CREATE TABLE IF NOT EXISTS `captcha_captchastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge` varchar(32) NOT NULL,
  `response` varchar(32) NOT NULL,
  `hashkey` varchar(40) NOT NULL,
  `expiration` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashkey` (`hashkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.django_admin_log 结构
CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_3798ebfe` FOREIGN KEY (`user_id`) REFERENCES `account_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.django_content_type 结构
CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.django_session 结构
CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.report_report 结构
CREATE TABLE IF NOT EXISTS `report_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` int(11) NOT NULL,
  `report_desc` varchar(500) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `report_date` datetime NOT NULL,
  `handled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_report_6340c63c` (`user_id`),
  KEY `report_report_56bb4187` (`subject_id`),
  KEY `report_report_3925f323` (`comment_id`),
  CONSTRAINT `comment_id_refs_id_821e768c` FOREIGN KEY (`comment_id`) REFERENCES `subject_comment` (`id`),
  CONSTRAINT `subject_id_refs_id_8803619b` FOREIGN KEY (`subject_id`) REFERENCES `subject_subject` (`id`),
  CONSTRAINT `user_id_refs_id_e633090c` FOREIGN KEY (`user_id`) REFERENCES `account_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.south_migrationhistory 结构
CREATE TABLE IF NOT EXISTS `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.sph_counter 结构
CREATE TABLE IF NOT EXISTS `sph_counter` (
  `counter_id` int(11) NOT NULL,
  `max_doc_id` int(11) NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.sph_tag_counter 结构
CREATE TABLE IF NOT EXISTS `sph_tag_counter` (
  `counter_id` int(11) NOT NULL,
  `max_doc_id` int(11) NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.subject_comment 结构
CREATE TABLE IF NOT EXISTS `subject_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `pub_date` datetime NOT NULL,
  `like` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject_comment_56bb4187` (`subject_id`),
  KEY `subject_comment_3925f323` (`comment_id`),
  KEY `subject_comment_6340c63c` (`user_id`),
  CONSTRAINT `comment_id_refs_id_1c81163d` FOREIGN KEY (`comment_id`) REFERENCES `subject_comment` (`id`),
  CONSTRAINT `subject_id_refs_id_403ac857` FOREIGN KEY (`subject_id`) REFERENCES `subject_subject` (`id`),
  CONSTRAINT `user_id_refs_id_770af874` FOREIGN KEY (`user_id`) REFERENCES `account_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.subject_comment_like_users 结构
CREATE TABLE IF NOT EXISTS `subject_comment_like_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `myuser_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `comment_id` (`comment_id`,`myuser_id`),
  KEY `subject_comment_like_users_3925f323` (`comment_id`),
  KEY `subject_comment_like_users_f1d9e869` (`myuser_id`),
  CONSTRAINT `comment_id_refs_id_76700dbf` FOREIGN KEY (`comment_id`) REFERENCES `subject_comment` (`id`),
  CONSTRAINT `myuser_id_refs_id_e837c160` FOREIGN KEY (`myuser_id`) REFERENCES `account_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.subject_subject 结构
CREATE TABLE IF NOT EXISTS `subject_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(140) NOT NULL,
  `url` varchar(200) NOT NULL,
  `netloc` varchar(200) NOT NULL,
  `content` varchar(500) NOT NULL,
  `pub_date` datetime NOT NULL,
  `public` tinyint(1) NOT NULL,
  `votes` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `url` (`url`),
  KEY `subject_subject_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_8ef0f3f6` FOREIGN KEY (`user_id`) REFERENCES `account_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.subject_subject_tags 结构
CREATE TABLE IF NOT EXISTS `subject_subject_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_id` (`subject_id`,`tag_id`),
  KEY `subject_subject_tags_56bb4187` (`subject_id`),
  KEY `subject_subject_tags_5659cca2` (`tag_id`),
  CONSTRAINT `subject_id_refs_id_1edd618c` FOREIGN KEY (`subject_id`) REFERENCES `subject_subject` (`id`),
  CONSTRAINT `tag_id_refs_id_41622dfa` FOREIGN KEY (`tag_id`) REFERENCES `subject_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.subject_subject_vote_users 结构
CREATE TABLE IF NOT EXISTS `subject_subject_vote_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `myuser_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_id` (`subject_id`,`myuser_id`),
  KEY `subject_subject_vote_users_56bb4187` (`subject_id`),
  KEY `subject_subject_vote_users_f1d9e869` (`myuser_id`),
  CONSTRAINT `myuser_id_refs_id_233eff89` FOREIGN KEY (`myuser_id`) REFERENCES `account_myuser` (`id`),
  CONSTRAINT `subject_id_refs_id_52a818b9` FOREIGN KEY (`subject_id`) REFERENCES `subject_subject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.subject_tag 结构
CREATE TABLE IF NOT EXISTS `subject_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `subject_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.website_about 结构
CREATE TABLE IF NOT EXISTS `website_about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `author_info` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 gongshare.website_feedback 结构
CREATE TABLE IF NOT EXISTS `website_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `contact` varchar(50) NOT NULL,
  `pub_date` datetime NOT NULL,
  `upload_file` varchar(100) DEFAULT NULL,
  `handled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
