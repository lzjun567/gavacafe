#!/bin/bash

NAME="gongshare_app"
DJANGODIR=/home/gongshare/gongshare
SOCKFILE=/tmp/gongshare.sock
USER=root
GROUP=root
NUM_WORKERS=3
DJANGO_SETTINGS_MODULE=gongshare.settings
DJANGO_WSGI_MODULE=gongshare.wsgi



echo "Starting $NAME as `whoami`"

cd $DJANGODIR
source /root/envs/gongshare/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

exec /root/envs/gongshare/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--log-level=error \
--bind=unix:$SOCKFILE
