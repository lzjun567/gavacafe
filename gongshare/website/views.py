# -*- encoding=UTF-8 -*-
import os.path

from django.shortcuts import render_to_response
from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from form import FeedbackForm
from django.conf import settings
from .models import About

def about(request):
    
    try:
        about = About.objects.all()[0]
    except IndexError:
        about = None
    return render_to_response('about.html',{'about':about},\
            context_instance = RequestContext(request)); 

def feedback(request):
    if request.method=='POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect("/")
    else:
        form = FeedbackForm()
        return render(request,'feedback.html',{'form':form,})

def changelog(request):
    "网站更新日志"
    root_path = os.path.realpath(os.path.dirname(__file__))
    with open(os.path.join(root_path, '../../changelog.md'), 'r') as f:
        content = ''.join(f.readlines())
    return render(request, 'changelog.html', {'content': content})
    
    
