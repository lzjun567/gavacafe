#! /usr/bin/env python

from django import forms

class FeedbackForm(forms.Form):
    content = forms.CharField(widget=forms.Textarea)
    contact = forms.EmailField()
    upload_file = forms.ImageField()
