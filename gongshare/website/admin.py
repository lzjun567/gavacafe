#-*- encoding=UTF-8 -*-

from django.contrib import admin

from .models import Feedback
from .models import About



class AboutAdmin(admin.ModelAdmin):
    pass

class FeedbackAdmin(admin.ModelAdmin):
    list_display=('handled','content','pub_date',)

admin.site.register(About,AboutAdmin)
admin.site.register(Feedback,FeedbackAdmin)
