# -*- encoding=utf8 -*-
from datetime import datetime

from django.db import models
from tinymce import models as tinymce_models

class About(models.Model):
    content =  tinymce_models.HTMLField() 
    author_info = tinymce_models.HTMLField()
    class Meta:
        verbose_name_plural = u'关于'

class Feedback(models.Model):
    content = models.TextField(u'反馈内容')
    contact = models.CharField(u'联系方式',max_length=50)
    pub_date = models.DateTimeField(u'提交时间',default=datetime.now)
    upload_file = models.CharField(max_length=50, null=True, blank=True)
    #upload_file = models.ImageField(upload_to='images/feedback',null=True,blank=True)
    handled = models.BooleanField(u'已处理',default=False)

    class Meta:
        verbose_name_plural = u'反馈'
