##-*- encoding=UTF-8 -*-
import time
from datetime import datetime

def calc_score(votes, item_hour_age, gravity=2.8):
    return (votes-1) / pow((item_hour_age+2), gravity)

def note_score(subject):
    '''根据subject的投票数计算分值，系统根据subject的分值排序显示
        param@subject:Subject对象
        return subjet的得分值
    '''
    votes = subject.votes
    hour_age = ((datetime.now() - subject.pub_date).total_seconds())/3600
    return calc_score(votes, hour_age)



