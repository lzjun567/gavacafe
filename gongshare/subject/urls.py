# -*-encoding=UTF-8 -*-
from django.conf.urls import patterns,url

urlpatterns = patterns('subject.views',
    url(r'^hot/$','subjects', name='hot'),
    url(r'^$','recent', name='index'),
    url(r'^recent/$', 'recent', name='recent'),
    url(r'^(\d+)/$','subject_detail',name='detail'),
    url(r'^tag_search2/$','tag_search2'),
    url(r'^add_subject/$','add_subject'),
    url(r'^(\d{1,9})/favour/$','subject_favour'),
    url(r'^(\d{1,9})/comment/add/$','add_comment'),
    url(r'^comment/(\d{1,9})/like/$','comment_like'),
    url(r'^ajax/$','subject_search_by_url'),
    url(r'^delete/(\d{1,9})/$','delete_subject'),
    url(r'^comment/(\d{1,9})/delete/$','delete_comment'),
    url(r'^(\d{1,9})/edit/$', 'edit_subject', name='edit'),
)
