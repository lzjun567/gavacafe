# -*- encoding=UTF-8 -*-
import urllib2
import json
import urlparse
import time
import re
from datetime import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import Http404
from django.http import HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.utils import simplejson
from django.db.models import Q
from django.core.mail import send_mail
from django.template import RequestContext
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.conf import settings
from django.utils.html import strip_tags
from django.db import IntegrityError

import rank
from .models import Subject
from .models import Comment
from .models import Tag
from .models import MyUser
from account.functions import ajax_login_required
from account.functions import normal_login_required
from form import SubjectForm
from weibo import APIClient
from publicsuffix import PublicSuffixList

from sphinx.sphinxapi import *
from paginator.Paginator import paginate

client = SphinxClient()
client.SetServer(settings.SPHNIX_SERVER,9312)
client.SetMatchMode(SPH_MATCH_EXTENDED2)

def search(request):
    """
    资源搜索
    """
    q = request.GET.get('q')
    subjects = [] 
    if q:
        matches = [] 
        query  = client.Query(q.strip(),'subject_main;subject_delta')
        if query:
            matches = query.get('matches')
            pks =[ match['id'] for match in matches]
            subjects = Subject.objects.filter(pk__in=pks)
        else:
            subjects = Subject.objects.filter(title__icontains=q)

        query = client.Query('^'+q.strip()+'$','tag_main;tag_delta')
        if query:
            matches = query.get('matches')
            if matches:
                tag = Tag.objects.get(id=matches[0].get('id'))
                from itertools import chain
                subjects = list(set(chain(subjects,tag.subject_set.all())))
                #subjects.append(tag.subject_set.all())
    return paginate('subject_list',subjects,request,'subject_list.html',query_key=q,count=len(subjects))

def subject_search_by_url(request):
    '''根据URL搜索资源'''
    url = request.GET.get('url')
    try:
        subj = Subject.objects.get(url=url)
        return json_response({'title':subj.title,'id':subj.id})
    except Subject.DoesNotExist:
        return json_response({}) 
    
def subjects(request):
    '''获取所有资源'''
    subject_list = Subject.objects.order_by('-pub_date')[:settings.LIMIT_SUBJECT_SORT]
    subject_list = sorted(subject_list, key=rank.note_score, reverse=True)
    return paginate('subject_list', subject_list, request, 'subject_list.html', home=True)

def recent(request):
    '''
    最近
    '''
    subject_list = Subject.objects.order_by('-pub_date')
    return paginate('subject_list', subject_list, request, 'subject_list.html', recent=True, home=True)

def subject_detail(request,subject_id):
    '''
    获取指定资源及评论
    '''
    try:
        subject_ = Subject.objects.get(id=int(subject_id))
        comments = Comment.objects.filter(subject=subject_)
        return render_to_response('subject_detail.html',{'subject':subject_,'comments':comments},\
                context_instance=RequestContext(request))
    except Subject.DoesNotExist:
        raise Http404


@normal_login_required
@csrf_exempt
def edit_subject(request,subject_id):
    """修改资源"""
    subject = Subject.objects.get(id = int(subject_id))
    login_user = request.session['user_']
    if subject.user == login_user:
        if request.method == "GET":
            return render_to_response("subject_edit.html",{'subject':subject},\
                    context_instance = RequestContext(request))
        elif request.method == "POST":
            content = request.POST.get('content')
            subject.content = content
            subject.save()
            return json_response({'msg':'success'})
    else:
        return HttpResponse('not_authenticated');

@csrf_exempt
@ajax_login_required
def subject_favour(request,subject_id):
    '''赞Subject'''

    subject = Subject.objects.get(id=int(subject_id)) 
    user_ = request.session['user_']
    method = request.GET.get('method')
    if method == 'vote_up':
        if user_ in subject.vote_users.all():
            return HttpResponse(simplejson.dumps({'msg':'favoured'}))#fixme
        else:
            subject.votes = subject.votes + 1
            subject.vote_users.add(user_)
            subject.save()
            return HttpResponse(simplejson.dumps({'msg':'success'}),mimetype='application/json')
    elif method == 'vote_down':
        if user_ not in subject.vote_users.all():
            return json_response({'msg':'disfavoured'})
        else:
            subject.votes = subject.votes - 1
            subject.vote_users.remove(user_)
            subject.save()
            return json_response({'msg':'success'})
    else:
        raise Http404

    
@csrf_exempt
@ajax_login_required
def comment_like(request,comment_id):
    '''赞评论'''
    comment = Comment.objects.get(id=int(comment_id))
    user_ = request.session['user_']

    if user_ in comment.like_users.all():
        return json_response({'msg':'liked'})
    else:
        comment.like = comment.like+1
        comment.like_users.add(user_)
        comment.save()
        return json_response({'msg':'success'})



client_tag = SphinxClient()
client_tag.SetServer(settings.SPHNIX_SERVER,9312)
client_tag.SetMatchMode(SPH_MATCH_EXTENDED2)
client_tag.SetSortMode(SPH_SORT_ATTR_DESC,'subject_count')
client_tag.SetLimits(0,8)

@csrf_exempt
def tag_search2(request):
    """
    标签搜索
    """
    query = request.GET.get('query','').strip()
    d = {}
    if query:
        patten = re.compile(u'[\u4e00-\u9fa5]+')
        match = patten.search(unicode(query.strip()))
        if not match and query.isalpha():
            query = query+"*"

        result= client_tag.Query(query,'tag_main;tag_delta')
        matches = []
        data = []
        if result:
            matches = result.get('matches')
            for match in matches:
                tag = Tag.objects.get(id=match['id'])
                element =  {'value':tag.title,'s_count':tag.subject_count}
                data.append(element)
        else:
            query = query.replace('*','')
            tags = Tag.objects.filter(title__startswith=query)
            for tag in tags:
                element =  {'value':tag.title,'s_count':tag.subject_count}
                data.append(element)

        d['suggestions']=data
    json = simplejson.dumps(d)
    return HttpResponse(json,mimetype="application/json")


@csrf_exempt
@ajax_login_required
def delete_subject(request,subject_id):
    '''根据指定id删除资源'''
    try:
        subj = Subject.objects.get(id = int(subject_id))
        if request.session['user_']==subj.user:
            for tag in subj.tags.all():
                tag.subject_count -= 1
                tag.save()
            subj.delete()
            return json_response({'success':True})
        else:
            return json_response({'success':False,'msg':u'没有权限'})
    except Subject.DoesNotExist:
        return json_response({'sucess':False,'msg':'该资源不存在'}) 
 

@csrf_exempt
@ajax_login_required
def add_subject(request):
    '''添加资源'''

    if request.method=="POST":
        form = SubjectForm(request.POST)
        if form.is_valid():
            form_url = form.cleaned_data['url'].strip()
            form_title = form.cleaned_data['title'].strip()
            form_content = form.cleaned_data['content'].strip()

            form_tags = form.cleaned_data['tags']
            user = request.session['user_']
            subject = Subject(title=form_title,url=form_url,content=form_content,pub_date=datetime.now(),\
                                votes = 1,netloc = PublicSuffixList().get_public_suffix(urlparse.urlparse(form_url)[1]),\
                                user = user)
            subject.save()
            for tag_title in form_tags:
                tag_obj = Tag.objects.get(title=tag_title)
                tag_obj.subject_count = tag_obj.subject_count + 1
                tag_obj.save()
                subject.tags.add(tag_obj)
        else:
            return json_response({
                'success': False,
                'errors': form.errors.values()})

        return json_response({'success':True,})
    else:
        raise  Http404

'''添加评论'''
@csrf_exempt
@normal_login_required
def add_comment(request,subject_id):

    subject_ = Subject.objects.get(id=int(subject_id))
    if request.method=="POST":
        comment_content = request.POST['content']
        comment_content = strip_tags(comment_content)
        from_comment_id = request.POST.get('comment_id')
        from_comment = None
        if from_comment_id:
            try:
                from_comment = Comment.objects.get(pk=int(from_comment_id))
            except Comment.DoesNotExist:
                pass

        comment = Comment(content = comment_content,user = request.session['user_'],subject=subject_,comment=from_comment)
        comment.save()
        return render_to_response('comment.html',{'comment':comment})

@csrf_exempt
@ajax_login_required
def delete_comment(request,comment_id):
    '''删除评论'''
    comment = Comment.objects.get(id=int(comment_id))
    user_ = request.session['user_']

    if user_ == comment.user:
        comment.delete()
        return json_response({'success':True})
    else:
        return json_response({'success':False,'msg':u'没有权限'})

def json_response(data):
    return HttpResponse(json.dumps(data,separators=(',',':')),
                        mimetype='application/json;charset=UTF-8')

