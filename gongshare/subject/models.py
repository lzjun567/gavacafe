#-*- encoding=UTF-8 -*-
from datetime import datetime

from django.db import models
from django import forms
from django.core.validators import MinValueValidator

from account.models import MyUser

class Subject(models.Model):
    '''
    主题类，用户提交的一条链接
    '''
    title=models.CharField(u"标题", max_length=140, db_index=True, unique=True)
    url = models.URLField(max_length=200, unique=True)
    netloc = models.CharField(max_length=200, blank=True, verbose_name=u'域名')
    content = models.CharField(max_length=500, blank=True, default='', verbose_name=u"内容")
    pub_date = models.DateTimeField(u"发布日期", default=datetime.now)
    tags = models.ManyToManyField('Tag', verbose_name='tag')
    public = models.BooleanField(u'公开', default=True)
    votes = models.IntegerField(default=0)#投票数
    user = models.ForeignKey(MyUser, related_name='post_subjects')
    vote_users = models.ManyToManyField(#投过票的用户
            MyUser, blank=True, null=True, 
            related_name='vote_subjects', verbose_name='投票人')
    def __hash__(self):
        return hash((self.title, self.url,))

    def __eq__(self, other):
        if isinstance(other, Subject):
            return (self.url, self.title)==(other.url, other.title)
        else:
            return False

    def __unicode__(self):
        return self.title

    def comment_count(self):
        return Comment.objects.filter(subject=self).count()

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('subject.view.subject_detail',args=[str(self.id)])

    class Meta:
        verbose_name_plural = u"主题"

class Tag(models.Model):
    '''标签'''
    title = models.CharField(max_length=50,unique=True)
    content = models.TextField(max_length=500,blank=True,default="")
    subject_count = models.IntegerField(default=0)
    
    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = u"标签"


class SubjectModelForm(forms.ModelForm):
    content=forms.CharField(widget=forms.Textarea,required=False)
    class Meta:
        model = Subject 

"""评论"""
class Comment(models.Model):
    content = models.CharField(max_length=500)
    subject = models.ForeignKey(Subject)
    comment = models.ForeignKey("self",related_name="replay_comment",blank=True,null=True)
    pub_date = models.DateTimeField("发布日期",default=datetime.now())
    like = models.IntegerField(validators = [MinValueValidator(0)],default=0)
    user = models.ForeignKey(MyUser,verbose_name="评论人",blank=True,null=True)
    like_users = models.ManyToManyField(MyUser,null=True,related_name='like_comments',verbose_name='投票人')

    def __unicode__(self):
        return self.content

    class Meta:
        verbose_name_plural = u'评论'

class CommentModelForm(forms.ModelForm):
    content = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Comment




