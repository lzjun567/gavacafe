#!/usr/bin/env python
#coding:utf-8

from django import template
from utils.timebefore import timebefore

register = template.Library()

@register.filter
def get_range(value):
    return range(value)

@register.filter
def sub_one(value):
    return value - 1

@register.filter
def sub_two(value):
    return value - 2 

@register.filter
def add_one(value):
    return value + 1

@register.filter
def add_two(value):
    return value +  2 
def timebefore_filter(value,arg=None):
    '''格式化时间，以距离当前时间xx来显示（如:2小时前）''' 
    if not value:
        return u''
    try:
        if arg:
            return timebefore(value,arg)
        return timebefore(value)
    except(ValueError,TypeError):
        return u''
    

register.filter('timebefore', timebefore_filter) 

#custom tag
class TestNode(template.Node):
    def __init__(self):
        pass
    
    def render(self,context):
        print context['user']
        return "xxxxx"

def test(parse,token):
    return TestNode()

register.tag('my_tag',test)



