#-*- encoding=UTF-8 -*-

from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from subject.models import Tag
from subject.models import MyUser
from subject.models import Subject
from subject.models import Comment
from subject.models import SubjectModelForm
from subject.models import CommentModelForm


class SubjectAdmin(admin.ModelAdmin):

    list_display=('id','title_link','pub_date','public','user',)
    search_fields=("title",)
    filter_horizontal = ('tags',)
    raw_id_fields = ('tags','user','vote_users',)
    ordering=('-pub_date',)
    actions = ['make_private_action']
    actions_on_top = True   
    form = SubjectModelForm
    list_per_page = 60

    def title_link(self,obj):
        return u'<a href="%s">%s</a>' % (obj.url,obj.title)
    title_link.allow_tags = True
    title_link.short_description = u"标题"

    def make_private_action(self,request,queryset):
        rows_updated = queryset.update(public=False)
        #self.message_user(request,"%s 条记录被更新" % rows_updated)

    make_private_action.short_description = u"标记所选subject为私有"

    def format_date(self,obj):
        return obj.pub_date.strftime('%Y-%m-%d %H:%M')

    format_date.short_description = u'发布日期'
    

class CommentAdmin(admin.ModelAdmin):
    list_display=('content','pub_date',)
    ordering = ('pub_date',)
    form = CommentModelForm

    def format_date(self,obj):
        return obj.pub_date.strftime('%Y-%m-%d %H:%M')

    format_date.short_description = 'Date'

class TagAdmin(admin.ModelAdmin):
    list_display=('id','title',)
    search_fields = ('id',"title",'content')

admin.site.register(Subject,SubjectAdmin)
admin.site.register(Comment,CommentAdmin)
admin.site.register(Tag,TagAdmin)

