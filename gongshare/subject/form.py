#!/usr/bin/env python
# -*-encoding=UTF-8 -*-
from django import  forms
from .models import Subject

class SubjectForm(forms.Form):
    url = forms.URLField(
            error_messages={
                'required':u'链接地址木有填写哦',
                'invalid':u'链接地址格式好像不正确哦'})
    title = forms.CharField(
            max_length=140,
            min_length=3,
            error_messages={
                'min_length':u'标题太短啦',
                'max_length':u'简洁的标题更能吸引注意力'})
    content = forms.CharField(
            max_length=500,
            required=False,
            error_messages={'invalid':u'字数太长啦，不要超过500'})
    tags = forms.CharField() 

    def clean_tags(self):
        tags = self.cleaned_data.get('tags','')
        t_tags = tags.split()
        if t_tags < 1:
            raise forms.ValidationError(u'至少选择一个标签')
        return  t_tags
    def clean_url(self):
        url = self.cleaned_data['url']
        try:
            subject = Subject.objects.get(url=url)
        except Subject.DoesNotExist:
            return url
        raise forms.ValidationError(u'该资源已经存在了')
    def clean_title(self):
        title = self.cleaned_data['title']
        try:
            subject = Subject.objects.get(title=title)
        except Subject.DoesNotExist:
            return title
        raise forms.ValidationError(u'该资源已经存在了')
        
