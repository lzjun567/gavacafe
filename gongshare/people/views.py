# -*- encoding=UTF-8  -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.conf import settings
from account.models import MyUser
from paginator.Paginator import paginate
from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from account.functions import ajax_login_required
import os,json

def index(request,user_id): 
    '''user_id的首页，默认显示该username发表的主题'''
    try:
        user = MyUser.objects.get(pk=user_id)
        subject_list = user.post_subjects.all().order_by('-pub_date')
        filename = os.path.split(user.avatar)[1]
        return paginate('subject_list',subject_list,request,'people.html',user=user,avatar=filename)
    except MyUser.DoesNotExist:
        raise Http404        

def favour(request,user_id): 
    '''user_id 赞过的主题'''
    try:
        user = MyUser.objects.get(pk=user_id)
        subject_list = user.vote_subjects.all().reverse()#.order_by('-id')
        filename = os.path.split(user.avatar)[1]
        return paginate('subject_list',subject_list,request,'people.html',user=user,avatar=filename,favoured=True)
    except MyUser.DoesNotExist:
        raise Http404

@csrf_exempt
@ajax_login_required
def edit_intro(request,user_id):
    content = request.POST.get('content')
    user = request.session['user_']
    user.signature = content
    user.save()
    data = {'msg':'success'}
    return HttpResponse(json.dumps(data,separators=(',',':')),
                        mimetype='application/json;charset=UTF-8') 
