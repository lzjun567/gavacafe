# -*-encoding=UTF-8 -*-
from django.conf.urls import patterns,url

urlpatterns = patterns('people.views',
    url(r'^(\d{1,9})/$', 'index', name='index'),
    url(r'^(\d{1,9})/favour$','favour'),
    url(r'^(\d{1,9})/edit_intro/$','edit_intro'),
)
