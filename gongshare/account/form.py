# -*- encoding=utf-8 -*-
import uuid

from django import forms
from django.contrib.auth import authenticate

from captcha.fields import CaptchaField
from .models import MyUser

class LoginForm(forms.Form):
    '''
    Fields for Login
    '''
    email = forms.EmailField(
            required=True, 
            error_messages={'invalid':u'邮箱格式不正确',
                            'required':u'邮箱地址不能为空'})
    password = forms.CharField(
            required=True,
            max_length=32,
            error_messages={'required':u'密码不能为空'},
            widget=forms.PasswordInput(render_value=False))

    def clean(self):
        '''
        用户名/密码认证
        '''
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        self._user = authenticate(email=email, password=password)
        if self._user is None:
            raise forms.ValidationError(u"Email或密码错误")
        return self.cleaned_data

    def save(self):
        return getattr(self, "_user")


class RegisterForm(forms.ModelForm):
    password = forms.CharField(
            required=True,
            max_length=32,
            error_messages={'required':u'密码不能为空'},
            widget=forms.PasswordInput(render_value=False))
    captcha = CaptchaField(error_messages={'required':u'验证码不能为空',
                                            'invalid': u'验证码不正确',})
    def __init__(self, *args, **kargs):
        """
        model 中的email字段是可以为空的，因为可以通过
        第三方平台登录，而对于此form表单，是通过本站
        登录凭证登录，所以email字段必须不能为空
        """
        super(RegisterForm, self).__init__(*args, **kargs)
        self.fields['email'].required = True
        self.fields['email'].error_messages = {'invalid':u'邮箱格式不正确',
                                                'required':u'邮箱地址不能为空'}

    class Meta:
        model = MyUser
        fields = ("email", "username")

    def clean_email(self):
        '''
        email 全局唯一
        '''
        email = self.cleaned_data.get('email')
        try:
            user = MyUser.objects.get(email=email)
        except MyUser.DoesNotExist:
            return email
        raise forms.ValidationError((u'email 已经被注册'), code='invalid')

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if len(password) < 6:
            raise forms.ValidationError((u'密码长度不能小于6个字符'), code='invalid')
        return password

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            user = MyUser.objects.get(username=username)
        except MyUser.DoesNotExist:
            return username
        raise forms.ValidationError((u'该用户名已经被注册'), code='invalid')

    def save(self, *args, **kwargs):
        user = super(RegisterForm, self).save(*args, **kwargs)
        password = self.cleaned_data.get('password')
        user.active_key = str(uuid.uuid1())
        if password:
            user.set_password(password)
        user.save()
        return user
