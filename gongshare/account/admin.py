from django.contrib import admin
from .models import MyUser

class UserAdmin(admin.ModelAdmin):
    list_display=('id','username',)
    ordering = ('username',)
    search_fields = ("username",)

admin.site.register(MyUser, UserAdmin)
