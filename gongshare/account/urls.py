# -*- encoding=utf-8 -*-
from django.conf.urls import patterns,url


urlpatterns = patterns("account.views",
            url(r'^login/(?P<login_type>\w+)/$', 'third_login', name='login'),
            url(r'^login/weibo/done/$', 'weibo_auth'),
            url(r'^github_login/$', 'github_login'),
            url(r'^github_callback', 'github_callback'),
            url(r'^weibo_login/$', 'weibo_login', name='weibo_login'),
            url(r'^logout/$', 'logout', name='logout'),
            url(r'^islogin/$', 'islogin'),
            url(r'^weibo_post/$', 'weibo_post'),
            url(r'^short_url/$', 'short_url'),
            url(r'^login/$', 'login', name="login"),
            url(r'^signup/$', 'signup', name="signup"),
            url(r'^captcha/$', 'form_valid'),
            url(r'^reg_confirm/(?P<uuid>\S{36})/$', 'user_active', name='user_active'),
            


       ) 
