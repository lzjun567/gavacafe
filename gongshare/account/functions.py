# -*- encoding=utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import render_to_response
import json

def ajax_login_required(view_function):
    def wrap(request,*arguments,**keywords):
        if request.session.get('user_'):
            return view_function(request,*arguments,**keywords)
        output = json.dumps({'not_authenticated':True})
        return HttpResponse(output,mimetype="application/json")

    wrap.__doc__ = view_function.__doc__
    wrap.__dict__ = view_function.__dict__

    return wrap

def normal_login_required(view_function):
    def wrap(request,*arguments,**keywords):
        if request.session.get('user_'):
            return view_function(request,*arguments,**keywords)
        

        
       # post_values = requset.POST.copy()  
       # post_values['authenticated']=False
        
        return HttpResponse('not_authenticated');

    wrap.__doc__ = view_function.__doc__
    wrap.__dict__ = view_function.__dict__

    return wrap
