#-*- encoding=UTF-8 -*-
from datetime import datetime

from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, 
                                        BaseUserManager, PermissionsMixin)
from django.utils import timezone

class MyUserManager(BaseUserManager):

    def create_user(self, username, email=None, password=None, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError(u'必须指定email')
        email = MyUserManager.normalize_email(email)
        user = self.model(username=username, email=email,
                            is_staff=False, is_active=True,
                            is_superuser=False, last_login=now,
                            **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        u = self.create_user(username, email, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.save(using=self._db)
        return u

class MyUser(AbstractBaseUser, PermissionsMixin):
    '''
    自定义的用户模块
    '''
    weibo_uid = models.CharField(max_length=100, blank=True, default='')
    username=models.CharField(max_length=50, unique=True, error_messages={'blank':u"用户名不能为空"})
    auth_token = models.CharField(max_length=100, blank=True, default='')
    expired_time = models.FloatField(blank=True, default=0.0, null=True)

    avatar = models.CharField(max_length=100, blank=True, default='')
    email = models.EmailField(unique=True, blank=True, null=True)
    add_time = models.DateTimeField(u"添加时间", default=datetime.now)
    signature = models.CharField(max_length=128, blank=True, default='')

    active_key = models.CharField(max_length=100 )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    is_staff = models.BooleanField('staff status', default=False, 
            help_text='designates whether whether the '
            'user can log into this site')
    is_active = models.BooleanField('active', default=False, help_text='Designates'
            ' whether this user should be treated active ')
    objects = MyUserManager()

    def get_full_name(self):
        full_name = '%s %s' % self.username
        return full_name.strip()
    
    def get_short_name(self):
        return self.username
     
    def __unicode__(self):
        return self.username

    class Meta:
        verbose_name_plural = u'账户'
