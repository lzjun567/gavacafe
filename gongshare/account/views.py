# -*- encoding=UTF-8 -*-
import urllib2
import urllib
import logging
import json
import uuid
import os
from StringIO import StringIO
import uuid
import datetime
import threading

from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from django.core.files.base import ContentFile
from django.db import models
from django.contrib.auth import (
        authenticate, 
        login as auth_login
    )
from django.template import RequestContext

from account.models import MyUser
from github import GitHub
from third_auth.weibo.weibo_wrapper import WeiboClient 
from account.functions import ajax_login_required
from third_auth.weibo.weibo import APIError
from third_auth.weibo.error import error_dict
from .form import LoginForm
from .form import RegisterForm
from tools.mail import send_mail
from captcha.models import CaptchaStore
from captcha.helpers import captcha_image_url
from utils.urls import login_redirect


@csrf_exempt
def signup(request, template='register.html'):
    form = RegisterForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        new_user = form.save()
        if not new_user.is_active:
            t = threading.Thread(
                    target=send_mail, args=(new_user.email, 
                    {'username':new_user.username, 'active_key':new_user.active_key}))
            t.start()
            return render(request, 
                        'signup_active.html', 
                        {'email':new_user.email}, context_instance=RequestContext(request))
    return render(request, template, {'form': form}, context_instance=RequestContext(request))

@csrf_exempt
def login(request, template="login.html"):
    form = LoginForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        authenticated_user = form.save()
        request.session['user_'] = authenticated_user
        auth_login(request, authenticated_user)
        return login_redirect(request)
    context = {"form":form}
    return render(request, template, context)

def user_active(request, uuid):
    users = MyUser.objects.filter(active_key=uuid)
    if users:
        user = users[0]
        if user.is_active:
            return render(request, "signup_active_after.html", {"status":0})
        if (user.add_time + datetime.timedelta(hours=0.5)) > datetime.datetime.now():
            user.is_active = True
            user.save()
            return render(request, "signup_active_after.html",{"status": 1, 'email':user.email})
        else:
            return render(request, "signup_active_after.html",{"status": -1})
    else:
        return render(request, "signup_active_after.html",{"status": -1})

        


def form_valid(request):
        if request.is_ajax():
            to_json_responce = dict()
            to_json_responce['status'] = 1
            to_json_responce['new_cptch_key'] = CaptchaStore.generate_key()
            to_json_responce['new_cptch_image'] = captcha_image_url(to_json_responce['new_cptch_key'])
            print to_json_responce
            return HttpResponse(json.dumps(to_json_responce), content_type='application/json')
        else:
            raise Http404

            
         

def _get_referer_url(request):
    referer_url = request.META.get('HTTP_REFERER','/')
    host = request.META['HTTP_HOST']
    if host not in referer_url and referer_url.startswith('http'):
        referer_url ='/'
    return referer_url

@csrf_exempt
def islogin(request):
    user = request.session.get("user_",None)

    if not user:
        json = simplejson.dumps('null')
        return HttpResponse(json,mimetype="application/json")
    else:
        json = simplejson.dumps(user.username)
        return HttpResponse(json,mimetype="application/json")
        


def third_login(request,login_type=None):
    if login_type=='weibo':
        return weibo_login(request)
    elif login_type=='github':
        return github_login(request)


def weibo_login(request):
    client = WeiboClient()
    url = client.get_authorize_url()
    request.session['forword_uri'] = _get_referer_url(request)
    return  HttpResponseRedirect(url)


def weibo_auth(request):
    if 'error' in request.GET or 'code' not in request.GET:
        return HttpResponseRedirect('/')
    code = request.GET['code']
    weibo_client = WeiboClient()
    r = weibo_client.request_access_token(str(code))
    weibo_client.set_access_token(r.access_token, r.expires_in)
    uid = r.uid
    u = weibo_client.client.users.show.get(uid=uid)
    try:
        user = MyUser.objects.get(weibo_uid=uid)
    except MyUser.DoesNotExist:
        url  = u['avatar_large']
        input_file = StringIO(urllib2.urlopen(url).read())
        avatar_path  = settings.MEDIA_ROOT+'/avatar/'

        if not os.path.exists(avatar_path):
            os.makedirs(avatar_path)

        name = str(uuid.uuid1()).replace('-','')+'.jpg'
        filename = os.path.join(avatar_path,name)

        with open(filename,'wb') as avatar_file:
            avatar_file.write(input_file.getvalue())

        user = MyUser(weibo_uid=uid,username = u['screen_name'],auth_token = r.access_token,\
                expired_time = r.expires_in,avatar=filename)

    user.auth_token = r.access_token
    user.expired_time = r.expires_in
    user.last_login = datetime.datetime.now()
    user.save()

    request.session['user_']=user
    referer_url = request.session.pop('forword_uri','/')
    if 'login' in referer_url:
        referer_url = '/'
    return HttpResponseRedirect(referer_url)


@csrf_exempt
@ajax_login_required
def weibo_post(request):
    user = request.session.get('user_','')
    if user:
        content = request.POST.get('content','')
        weibo_client = WeiboClient()
        weibo_client.set_access_token(user.auth_token,user.expired_time)
        try:
            weibo_client.weibo_post(content)
            return HttpResponse(json.dumps({'success':True}),mimetype='application/json')
        except APIError,e:
            return HttpResponse(json.dumps({'success':False,'error':error_dict.get(e.error_code,'u未知错误')}),mimetype='application/json')


def short_url(request):
    url = request.GET.get('url','')
    weibo_client = WeiboClient()    
    user = request.session.get('user_','')
    weibo_client.set_access_token(user.auth_token,user.expired_time)

    short_url = weibo_client.shortten(url)

    json = simplejson.dumps(short_url)
    return HttpResponse(json,mimetype="application/json")
        


def callback(request):
        
    APP_KEY = "4024038459"
    APP_SECRET = "ac0c58469ad89b03472e62f5f8598314"
    CALLBACK_URL = "http://127.0.0.1/callback"
    
    #client = APIClient(app_key=APP_KEY,app_secret=APP_SECRET,
    #                    redirect_uri=CALLBACK_URL)
    code =  request.GET['code']
    r = client.request_access_token(str(code))
    access_token = r.access_token
    expires_in = r.expires_in
    client.set_access_token(access_token,expires_in)
    _id = client.account.get_uid.get()
    user =  client.users.show.get(uid=_id['uid'])
    request.session['user_']=user['screen_name']
    return HttpResponseRedirect("/subject")



#登出
def logout(request):
    if 'user_' in request.session:
        del request.session['user_']
    return HttpResponseRedirect("/subject")

def github_login(request):
    client = GitHub(client_id='3d21018583cbcd39d0e9',
                    client_secret='89b4204aab07ea17606566a7e9295ec2cfc0c27b',
                    redirect_uri="http://127.0.0.1/github_callback")

    url = client.authorize_url(state='a-random-string')
    return HttpResponseRedirect(url)


def github_callback(request):

    APP_KEY = "3d21018583cbcd39d0e9"
    APP_SECRET = "89b4204aab07ea17606566a7e9295ec2cfc0c27b"
    CALLBACK_URL = "http://127.0.0.1/github_callback"
    
    client = GitHub(client_id='3d21018583cbcd39d0e9',
                    client_secret='89b4204aab07ea17606566a7e9295ec2cfc0c27b',
                    redirect_uri="http://127.0.0.1/github_callback")
    
    code =  request.GET['code']
    state = request.GET['state']
    access_token = client.get_access_token(str(code),str(state))
    gh = GitHub(access_token=access_token)
    user =  gh.user.get()

    #client.set_access_token(access_token)
    #id = client.account.get_uid.get()
    #user =  client.users.show.get(uid=id['uid'])
    print user['login']
    request.session['user_']=user['login']
    return HttpResponseRedirect("/subject")






