# -*- encoding=UTF-8 -*-
import requests
from django.template import Template
from django.template import Context
from django.conf import settings

def send_mail(email, context=None):
    context['site'] = settings.SITE
    content = '''Hi {{username}}：

    欢迎您注册gongshare，请点击以下链接完成注册：
                        {{site}}/account/reg_confirm/{{active_key}}

    gongshare是面向程序员的纯技术分享网站，分享主题可以是前端开发、后端开发、Linux内核技术等（技术无界限），你可以把自己的文章或者你看到的好文与大家分享。
    每天愉悦地读or分享一篇文章，让心灵少一分浮躁。

    系统发信，请勿回复
    gongshare官方网站：http://gongshare.com
    '''
    t = Template(content)
    c = Context(context)
    content = t.render(c)
    return requests.post(
        "https://api.mailgun.net/v2/gongshare.com/messages",
        auth=("api", "key-5i20jt29pmfxnn36fg4llkz7bx3zlu99"),
        data={"from": "gongshare <service@gongshare.com>",
              "to": [email ],
              "subject": "gongshare 注册激活邮件",
              "text": content})
