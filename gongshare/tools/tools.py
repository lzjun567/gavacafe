# -*-encoding:utf-8 -*-

import MySQLdb
import urlparse
from publicsuffix import PublicSuffixList
import requests
import urllib2
from bs4 import BeautifulSoup
import re
from datetime import datetime
from datetime import timedelta
import urllib
import time
import sys  
import random
import uuid
reload(sys)  
sys.setdefaultencoding("utf8") 

def get_netloc(url):
    netloc = urlparse.urlparse(url)[1]
    psl = PublicSuffixList()
    netloc =  psl.get_public_suffix(netloc)
    return netloc


def modifydata():
    #建立连接
    conn = MySQLdb.connect(host='localhost',user='root',passwd='',db='gavacafe',charset='utf8')
    #获取游标
    cursor = conn.cursor()
    cursor.execute('select * from  subject_subject')
    
    for result in cursor.fetchall():
        _id = result[0]
        url = result[2]
        psl = PublicSuffixList()
        netloc = urlparse.urlparse(url)[1]
        netloc =  psl.get_public_suffix(netloc)
        cursor.execute('update subject_subject set netloc="%s" where id="%s"' % (netloc,_id))
    
    conn.commit()
    conn.close()



#建立连接
conn = MySQLdb.connect(host='localhost',user='root',passwd='',db='gongshare',charset='utf8')
conn2 = MySQLdb.connect(host='localhost',user='root',passwd='',db='gongshare1',charset='utf8')


def migrate():
    try:
        cursor = conn.cursor()
        cursor.execute('select * from account_user')
        values = cursor.fetchall()
        for value in values:
            value = list(value)
            value[6] = str(uuid.uuid4())
            value.append(1)
            value = tuple(value)
            cursor2 = conn2.cursor()
            cursor2.execute('insert into account_myuser\
                        (id, weibo_uid, username, auth_token,expired_time, avatar,email,add_time,signature,is_active)\
                        values (%s, %s, %s, %s, %s, %s, %s,%s,%s, %s)', value)
            conn2.commit()

        cursor.execute('select * from django_content_type')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            cursor2.execute('insert into django_content_type values (%s, %s ,%s ,%s)',value)
            conn2.commit()
        
        cursor.execute('select * from auth_permission')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            cursor2.execute('insert into auth_permission values (%s, %s ,%s ,%s)',value)
            conn2.commit()
       
       
        cursor.execute('select * from sph_subject_counter')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into sph_counter values ('+s+')', value)
            conn2.commit()

        cursor.execute('select * from sph_tag_counter')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into sph_tag_counter values ('+s+')', value)
            conn2.commit()

        cursor.execute('select * from subject_subject')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into subject_subject values ('+s+')', value)
            conn2.commit()


        cursor.execute('select * from subject_tag')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into subject_tag values ('+s+')', value)
            conn2.commit()

        cursor.execute('select * from subject_subject_tags')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into subject_subject_tags values ('+s+')', value)
            conn2.commit()


        cursor.execute('select * from subject_subject_vote_users')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into subject_subject_vote_users values ('+s+')', value)
            conn2.commit()


        cursor.execute('select * from website_about')
        values = cursor.fetchall()
        for value in values:
            cursor2 = conn2.cursor()
            s = "%s,"*len(value)
            s = s[:len(s)-1]
            cursor2.execute('insert into website_about values ('+s+')', value)
            conn2.commit()
    except Exception, e:
        print e

if __name__ == '__main__':
    migrate()



def read_subject_data():
    cursor = conn2.cursor()
    try:
        cursor.execute('select * from subject_subject1')
        values = cursor.fetchall()
    except MySQLdb.Error,e:
        print e

    for value in values:
        try:
            print value[3]
            cursor = conn.cursor()
            cursor.execute('insert into subject_subject '
                            '(id,title,url,content,pub_date,votes,user_id,netloc,public) values'
                            '(%s,%s,%s,%s,%s,%s,%s,%s,%s)',value)
            conn.commit()
        except Exception ,e:
            print e

def read_tag_data():
    cursor = conn2.cursor()
    try:
        cursor.execute('select * from subject_tag')
        values = cursor.fetchall()
    except MySQLdb.Error,e:
        print e

    for value in values:
        try:
            cursor = conn.cursor()
            cursor.execute('insert into subject_tag '
                            'values(%s,%s,%s,%s)', value)
            conn.commit()
        except Exception,e:
            print e

def read_subject_tag_data():
    cursor = conn2.cursor()
    try:
        cursor.execute('select * from subject_subject_tags')
        values = cursor.fetchall()
    except MySQLdb.Error,e:
        print e

    for value in values:
        try:
            cursor = conn.cursor()
            cursor.execute('insert into subject_subject_tags '
                            'values(%s,%s,%s)', value)
            conn.commit()
        except Exception,e:
            print e


def insert_into_tag(value):
    print value.get('title','')

    cursor = conn.cursor()
    try:
        cursor.execute('insert into subject_tag (title,content) values\
                    (%s,%s)',(value.get('title',''),value.get('content','')))
        conn.commit()
    except MySQLdb.Error,e:
        print e

def insert(value):
    try:
        print value['title']
    except UnicodeEncodeError ,e:
        print e
    #获取游标
    cursor = conn.cursor()
    cursor.execute('insert into subject_subject (title,url,content,pub_date,votes,netloc,user_id) values\
            (%s,%s,%s,%s,%s,%s,%s)',(value['title'],\
            value['url'],value['content'],value['pub_date'],value['votes'],value.get('netloc',""),value['user_id']))
    conn.commit()


#url = 'http://news.dbanotes.net'
url = 'https://news.ycombinator.com'
params = None
def sratch_from_startup(local_url,param=None):
    if param:
        params= urllib.urlencode(param)
        response = urllib2.urlopen(local_url+'/x?%s' % params,timeout=40)
        print response.geturl()
    else:
        response = urllib2.urlopen(local_url,timeout=40)
        print response.geturl()
    soup = BeautifulSoup(response.read())
    
    subject= {} 
    for td in soup.find_all('td'):
        if  td.get('class')==['title']:
            if td.a:
                subject['url']=td.a.get('href')
                title = td.a.string
                subject['title'] = title
                if td.span:
                     subject['netloc'] = td.span.string.replace('(','').replace(')','')
                if td.parent.next_sibling:
                    info = td.parent.next_sibling.get_text()
                    matchObj = re.match(ur'([\d]+) points by [\w\u4e00-\u9fa5]* ([\d]+) ([\w]+) * ago',info,re.M|re.I)
                    if matchObj:
                        subject['votes'] = int(matchObj.group(1))
                        str_delta = matchObj.group(2)
                        if 'day' in info:
                            subject['pub_date'] = datetime.now()-timedelta(days=int(matchObj.group(2)))
                        elif 'hour' in info:
                            subject['pub_date'] = datetime.now()-timedelta(hours = int(matchObj.group(2)))
                        elif 'minute'in info:
                            subject['pub_date'] = datetime.now()-timedelta(minutes=int(matchObj.group(2)))
                        subject['user_id']=1
                        subject['content']=''
                        insert(subject)
                else:
                    param = {'fnid':td.a.get('href')[8:]}
                    print param['fnid']
                    if param['fnid']:
                        sratch_from_startup(url,param)
                    else:
                        sratch_from_startup('https://news.ycombinator.com/news2')

def sratch_from_csdn(page=1):

    csdn_url = 'http://geek.csdn.net/hot/'+str(page)
    response = urllib2.urlopen(csdn_url)
    #print response.read()
    soup = BeautifulSoup(response.read())
    print soup.original_encoding
    
    
    for li in soup.find_all('li'):
        subject = {}
        if li.has_attr('id'):
            subject['votes'] = int(li.a.string)
            subject['title'] = li.div.h4.a.string
            subject['url'] = li.div.h4.a.get('href')
            subject['netloc'] = get_netloc(subject['url'])
            subject['pub_date'] = datetime.now() - timedelta(days=random.randint(0,10))
            subject['user_id'] = 2
            subject['content']=''
            insert(subject)
    page = page +1
    if page < 23:
        sratch_from_csdn(page)
        






def sratch_tag_from_so(param=None):
    so_url = 'http://stackoverflow.com/tags'
    params = urllib.urlencode(param)
    response = urllib2.urlopen(so_url+"?%s"%params)
    print response.geturl()
    #html = "<td class='tag'><a href='/tag/android'  rel='tag'><img src='http://127.0.0.1/idf2.png' >android</a></td>"

    soup = BeautifulSoup(response.read())
    #soup = BeautifulSoup(html)
    for td in soup.find_all('td'):
        if td.a:
            insert_into_tag({'title':td.a.text.strip(),'content':''})




def relate_tag_and_subject():
    cursor = conn.cursor()
    cursor.execute('select id from subject_subject')
    results = cursor.fetchall()
    sub_size = len(results)
    subject_id = results[random.randint(1,sub_size)][0]

    cursor.execute('select id from subject_tag')

    tag_results = cursor.fetchall()
    tag_size = len(tag_results)
    tag_id = tag_results[random.randint(1,tag_size)][0]
    
    print subject_id,tag_id
    cursor.execute('insert into subject_subject_tags (subject_id,tag_id) values (%s,%s)',(subject_id,tag_id))

    #for  result in tuple(results):
    #    print result
    conn.commit()





