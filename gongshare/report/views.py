#-*- encoding=UTF-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from subject.models import Comment,Subject
from report.models import Report
import json

@csrf_exempt
def report(request):
    entity_id = request.POST.get('id')
    reason = request.POST.get('reason')
    report_type = request.POST.get('type')
    
    print reason
    reason_desc = request.POST.get('reason_desc')
    if report_type == 'subject':
        subject = subject=Subject.objects.get(pk=int(entity_id))
        comment = None
    else:
        subject = None
        comment = Comment.objects.get(pk=int(entity_id))

    report = Report(reason= int(reason),report_desc=reason_desc,subject=subject,\
                    comment=comment,user=request.session.get('user_'))  
    report.save()
    return json_response({'success':True})

def json_response(data):
    return HttpResponse(json.dumps(data,separators=(',',':')),
                        mimetype='application/json;charset=UTF-8')
