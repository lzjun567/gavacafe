#-*- encoding=UTF-8 -*-

from django.contrib import admin
from django.core.urlresolvers import reverse
from .models import Report


class ReportAdmin(admin.ModelAdmin):
    list_display =('id','handled','reason','subject_link','comment_link','report_desc','user','report_date')
    raw_id_fields = ('subject','user','comment',)
    ordering = ('report_date',)

    def subject_link(self,obj):
        link = (reverse('admin:subject_subject_change',args=(obj.subject.pk,)),obj.subject.title) if obj.subject else ("",None)
        return '<a href="%s">%s</a>' % link
    subject_link.allow_tags = True

    def comment_link(self,obj):
        link = (reverse('admin:subject_comment_change',args=(obj.comment.pk,)),obj.comment.content) if obj.comment else ("",None)
        return '<a href="%s">%s</a>' % link
    comment_link.allow_tags = True
    comment_link.short_description=u"主题"

admin.site.register(Report,ReportAdmin)
