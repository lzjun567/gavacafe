#-*- encoding=UTF-8 -*-
from datetime import datetime

from django.db import models
from account.models import MyUser
from subject.models import Subject
from subject.models import Comment

class Report(models.Model):
    '''举报'''
    REPORT_REASON = (
            (101,'广告垃圾信息'),
            (102,'骚扰谩骂'),
            (103,'敏感政治内容'),
            (104,'病毒木马'),
            (105,'其它'),
            )
    reason = models.IntegerField('举报原因',choices = REPORT_REASON,default=REPORT_REASON[4][0])
    report_desc = models.CharField('描述说明',max_length=500,blank=True,default='')
    user = models.ForeignKey(MyUser,verbose_name=u"举报人",null=True,blank=True)
    subject = models.ForeignKey(Subject,verbose_name=u'举报主题',null=True,blank=True)
    comment = models.ForeignKey(Comment,verbose_name=u'举报评论',null=True,blank=True)
    report_date = models.DateTimeField(u'举报时间',default=datetime.now)
    handled = models.BooleanField(u'已处理',default=False)

    class Meta:
        verbose_name_plural = u'举报'

def add_link_field(target_model = None, field = '', link_text = unicode):
    def add_link(cls):
        reverse_name = target_model or cls.model.__name__.lower()
        def link(self, instance):
            app_name = instance._meta.app_label
            reverse_path = "admin:%s_%s_change" % (app_name, reverse_name)
            link_obj = getattr(instance, field, None) or instance
            url = reverse(reverse_path, args = (link_obj.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, link_text(link_obj)))
        link.allow_tags = True
        link.short_description = reverse_name + ' link'
        cls.link = link
        cls.readonly_fields = list(getattr(cls, 'readonly_fields', [])) + ['link']
        return cls
    return add_link


