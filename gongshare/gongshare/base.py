#-*- encoding=UTF-8 -*-
import sys
import os.path
import time

DEBUG = True
TEMPLATE_DEBUG = DEBUG

PROJECT_PATH = os.path.dirname(os.path.realpath(os.path.dirname(__file__)))     
SITE_ROOT = os.path.dirname(PROJECT_PATH)

print SITE_ROOT

ADMINS = ()
MANAGERS = ADMINS
APPEND_SLASH = True

TIME_ZONE = 'Etc/GMT%+-d'%(time.timezone/3600)
LANGUAGE_CODE = 'zh-cn'
SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = False

MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(SITE_ROOT, 'static')
STATIC_URL = '/static/'
STATIC_PATH = './static/'
STATICFILES_DIRS = (
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'gongshare.urls'
WSGI_APPLICATION = 'gongshare.wsgi.application'

TEMPLATE_DIRS = (
        os.path.join(os.path.dirname(os.path.dirname(__file__)),'templates').replace('\\','/'),
        os.path.dirname(os.path.dirname(SITE_ROOT)).replace('\\','/'),
        os.path.join(SITE_ROOT, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.markup',

    'subject',
    'account',
    'website',
    'tinymce',
    'report',
    'captcha',
    'south',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

TINYMCE_DEFAULT_CONFIG = {'theme': "advanced", 'relative_urls': False}

# Turn off south during test
SOUTH_TESTS_MIGRATE = False

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "account.processor.tag_user",
)        

AUTH_USER_MODEL = 'account.MyUser'
SITE = 'http://gongshare.com'

#分页大小
PAGINATOR_PER_PAGE = 27
#对最近的指定数量的主题排序
LIMIT_SUBJECT_SORT = 1000
#Sphinx 服务器地址
SPHNIX_SERVER = 'localhost' 
