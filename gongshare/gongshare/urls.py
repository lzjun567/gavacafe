from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
     url(r'^my_admin/', include(admin.site.urls)),
     url(r'^$', include('subject.urls', namespace='index')),
     url(r'^search/$', 'subject.views.search'),
     url(r'^subject/', include('subject.urls', namespace='subject')),
     url(r'^account/', include('account.urls', namespace='account')),
     url(r'^people/', include('people.urls', namespace='people')),

     url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',{'document_root':settings.STATIC_PATH}),
     url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT}),
     url(r'^tinymce/', include('tinymce.urls')),

     url(r'^report/', include('report.urls')),
     url(r'^about/$', 'website.views.about', name='about'),
     url(r'^feedback/$', 'website.views.feedback'),
     url(r'^changelog/$', 'website.views.changelog', name='changelog'),
)

urlpatterns += patterns('',
        url(r'^captcha/', include('captcha.urls'),)
)
