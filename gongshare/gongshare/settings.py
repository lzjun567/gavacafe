#-*- encoding=UTF-8 -*-
import os
import os.path
import time

SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [   #django1.5必须在debug=false的时候添加受信任的IP
        '127.0.0.1',
        'gongshare.com',
	    'www.gongshare.com',
        'fav.foofish.net',
        '106.186.27.60',
	'162.243.150.120',
        ]
SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'#1.6 default is JSONSerializer

ADMINS = ()
MANAGERS = ADMINS
APPEND_SLASH = True
#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.mysql',
#        'NAME': 'gongshare',
#        'USER': 'root',     
#        'PASSWORD': '****'      
#        'HOST': '',           
#        'PORT': '',            
#    }
#}
TIME_ZONE = 'Etc/GMT%+-d'%(time.timezone/3600)
LANGUAGE_CODE = 'zh-cn'
SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = False
MEDIA_ROOT = '/home/gongshare/gongshare/media'

MEDIA_URL = '/media/'

STATIC_ROOT = '/home/gongshare/gongshare/static'

STATIC_URL = '/static/'

STATIC_PATH = './static'

STATICFILES_DIRS = (
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'ias$-!a$is=h)xkbq(p3hh8_*w-40ar5!x(@)+1ixf@mm1om4!'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',#支持本地化
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'gongshare.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'gongshare.wsgi.application'

TEMPLATE_DIRS = (
        os.path.join(os.path.dirname(os.path.dirname(__file__)),'templates').replace('\\','/'),
        os.path.dirname(os.path.dirname(SITE_ROOT)).replace('\\','/'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    #'django.contrib.markup',
    'subject',
    'account',
    'website',
    'tinymce',
    'report',
    'captcha',
    'south',#数据库迁移工具
)

TINYMCE_DEFAULT_CONFIG = {'theme': "advanced", 'relative_urls': False}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "account.processor.tag_user",
)        

AUTH_USER_MODEL = 'account.MyUser'
SITE = 'http://fav.foofish.net'
ENABLE_WEIBO_ACCOUNT=True
#WEIBO_API = {
#        'app_key':'*******************',
#        'app_secret':'*****************',
#        'redirect_uri':'/account/login/weibo/done'
#        }
#
WEIBO_AUTH_ENDPOINT='https://api.weibo.com/oauth2/authorize'
WEIBO_ACCESS_TOKEN_ENDPOINT='https://api.weibo.com/oauth2/access_token'
#WEIBO_REDIRECT_URI = '%s/account/login/weibo/done'%SITE 
WEIBO_OAUTH_VERSION=2
WEIBO_API_ENDPOINT = 'https://api.weibo.com/%d/' % WEIBO_OAUTH_VERSION


#分页大小
PAGINATOR_PER_PAGE = 27

#对最近的指定数量的主题排序
LIMIT_SUBJECT_SORT = 1000
#Sphinx 服务器地址
SPHNIX_SERVER = 'localhost' 



#EMAIL_HOST='smtp.gmail.com'
#EMAIL_PORT='25'
#EMAIL_HOST_USER='lzjun567@gmail.com'
#EMAIL_HOST_PASSWORD=''
#EMAIL_USE_TLS=True
#=======
#
#INTERNAL_IPS=('127.0.0.1',)
#
#
#DEBUG_TOOLBAR_PANELS = (
#    'debug_toolbar.panels.version.VersionDebugPanel',
#        'debug_toolbar.panels.timer.TimerDebugPanel',
#            'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
#                'debug_toolbar.panels.headers.HeaderDebugPanel',
#                    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
#                        'debug_toolbar.panels.template.TemplateDebugPanel',
#                            'debug_toolbar.panels.sql.SQLDebugPanel',
#                                'debug_toolbar.panels.signals.SignalDebugPanel',
#                                    'debug_toolbar.panels.logger.LoggingPanel',
#                                    )

try:
    from local_settings import *
except ImportError:
    pass
