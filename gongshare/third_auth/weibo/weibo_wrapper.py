from django.conf import settings
import weibo


class WeiboClient():

    def __init__(self,app_key=settings.WEIBO_API['app_key'],\
                    app_secret = settings.WEIBO_API['app_secret'],\
                    redirect_uri = settings.SITE+settings.WEIBO_API['redirect_uri'],\
                    access_token=None,expires=None
                    ):
        self.app_key = app_key
        self.app_secret = app_secret
        self.redirect_uri = redirect_uri
        self.client = weibo.APIClient(self.app_key,self.app_secret,self.redirect_uri)
        self.client.access_token = access_token
        self.client.expires = expires
        print 'uri:',redirect_uri

    def get_authorize_url(self):
        return self.client.get_authorize_url()

    def request_access_token(self,code):
        return self.client.request_access_token(str(code))

    def set_access_token(self,access_token,expires_in):
        return self.client.set_access_token(access_token,expires_in)


    def weibo_post(self,content):
        return self.client.statuses.update.post(status=content)

    def shortten(self,url):
        return self.client.short_url.shorten.get(url_long=url)

        
