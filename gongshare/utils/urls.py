# -*- encoding=utf-8 -*-
from django.shortcuts import redirect

def login_redirect(request):
    """
    跳转至登录或注册成功前的链接
    - next param
    """ 
    next = request.REQUEST.get("next", "/")
    return redirect(next)

