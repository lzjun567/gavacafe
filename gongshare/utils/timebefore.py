# -*- encoding=UTF-8 -*-
import datetime
def timebefore(d):
    '''时间d距离now()的长度，比如：1分钟前，1小时前，1月前，1年前'''
    chunks = (
        (60 * 60 * 24 * 365, u'年'),
        (60 * 60 * 24 * 30,  u'月'),
        (60 * 60 * 24 * 7, u'周'),
        (60 * 60 * 24, u'天'),
        (60 * 60, u'小时'),
        (60, u'分钟'),
    )

    #如果不是datetime类型转换后与datetime比较
    if not isinstance(d, datetime.datetime):
        d = datetime.datetime(d.year,d.month,d.day)
    now = datetime.datetime.now()
    delta = now - d
    #忽略毫秒
    before = delta.days * 24 * 60 * 60 + delta.seconds
    #刚刚过去的1分钟
    if before <= 60:
        return u'刚刚'
    for seconds,unit in chunks:
        count = before // seconds
        if count != 0:
            break
    if count == 1 and unit ==u'天':
        return u'昨天'
    elif count ==2 and unit == u'天':
        return u'前天'
    return unicode(count)+unit+u"前"

    



