$(function(){
var weibo_share_link = "http://gongshare.com/subject/";    
/*检查登录*/
function check_authentication(parsed_json){
    if(parsed_json.not_authenticated)
        return false;
    return true;
}
/* URL连接*/
function replaceURLWithHTMLLinks(text) {
     var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
     return text.replace(exp,"<a href='$1'>$1</a>"); 
}

/*登录弹出框*/
function signup(){
$.Zebra_Dialog('<div class="signup-form"><div class="button-row">'+
                            '<a class="sina-button submit-button" href="/account/login/weibo">新浪微博登录</a>'+
                            '<!--<a class="github-button  submit-button" href="#">GITHUB登录</a>-->'+
                            '</div>'+
                            '<div class="sign-intro"> <ul>'+
                            '<li>加入公舍：</li>'+
                            '<li><strong>发现、分享、传播</strong></li>'+
                            '</ul></div></div>',
            {
                title:'登录',
                overlay_close:false,
                type:'infomation',
                //position:['center','top+50'], //
                buttons:[],
                cancel:null,
            });
};
/*登录下拉菜单*/
var popup;
var nav = $("#nav-wrapper");
nav.delegate(".has_nav_menu", "click", function (c) {
    c.preventDefault();
    var a = $(this);
    var b = a.parent();
    if (popup) {
        popup.parent().removeClass("more-active");
        if ($.contains(b[0], popup[0])) {
            popup = null;
            return false
        }
    }
    b.addClass("more-active");
    popup = b.find(".more-items");
    popup.trigger("moreitem:show");
    return false
});
$(document).click(function (a) {
    if ($(a.target).closest(".more-items").length) {
        return
    }
    if (popup) {
        popup.parent().removeClass("more-active");
        popup = null
    }
});
/*tips*/
$('.up').tipsy(); 

/* 回复功能：有人赞时显示"赞" */
$('.zm-comment-ft').find('em').each(function(){
    if($(this).html()==0){$(this).parent().addClass('nil');}     
});
$('input[name=reason]').live('change',function(){
    $('.form-share').html('');
});
/*举报*/
function report(id,type){  //type：subject or comment
    var result = false;
    $.Zebra_Dialog('<div class="report-dialog-content"><form><ul class="options clearfix"><li><label><input value="101" name="reason" type="radio"> 广告垃圾信息</label></li><li><label><input value="102" name="reason" type="radio"> 骚扰谩骂 </label></li><li><label><input value="103" name="reason" type="radio"> 政治敏感内容 </label></li><li><label><input value="104" name="reason" type="radio"> 病毒木马</label></li><li><label><input value="105" name="reason" type="radio"> 其他</label></li></ul></form><div class="report-dialog-desc"><p>举报说明（可选）：<p><input type="text" class="reason_desc" maxlength="100" placeholder="描述恶意行为"></div></div>',
   {
       title:'为什么要举报此信息',
       overlay_close:false,
       type:'infomation',
       width:350,
       mode:true,
       buttons:[{caption:"确定",callback:function(){
           var reason = $('input[name=reason]:checked', 'form').val();
           if (typeof(reason)=='undefined'){
                 $('.form-share').html('<span style="color:#EE5151">您必须选择一项举报理由</span>');
            return
           }
           var report_desc = $('.reason_desc').val();
           var url = null;
           var data= {'id':id,'reason':reason,'reason_desc':report_desc};
           if(type=='subject'){
                data.type = 'subject'; 
           }else{
               data.type = 'comment';
           }
           $.ajax({type:'POST',url:'/report/',async:false,
                    data:data,
                    success:function(data){
                    if(!data.success){
                         alert(data.msg);    
                         result = false;
                    }else{
                        result = true;
                    }
                }});
            return result; 
       }}],
       onClose:function(caption){
            if(result){
                $.Zebra_Dialog('<div class="report-dialog-return">举报成功，我们会及时处理</div>',
                    {   'auto_close':2000,
                        'buttons':false,
                        'title':'举报',
                    });
            }}
   });
};

/*隐藏按钮操作*/
$('.subject-item-meta > a').bind('click',function(){
    //微博分享
    if($(this).attr('name')=='share'){
        var id = $(this).closest('.subject-item').attr('data-id');
        var url = weibo_share_link+id;
        var a = $(this).closest('.subject-item').children('.answer-head').find('a');
        var title = a.find('strong').html();
        weibo_share(title,url); 

    //举报
    }else if($(this).attr('name')=='report'){
        var subject_item = $(this).closest('.subject-item');
        var subject_id = subject_item.attr('data-id');
        report(subject_id,'subject');//TODO
    }else if($(this).attr('name')=='delete'){
        var subject_item = $(this).closest('.subject-item');
        var subject_id = subject_item.attr('data-id');
        var result = true;
        $.Zebra_Dialog('<div id="content"><p style="font-size:14px">一定要删除该资源吗？</p></div>',
            {title:"删除资源",buttons:[{caption:"确定",callback:function(){
                $.ajax({type:'POST',url:'/subject/delete/'+subject_id+'/',data:{},
                 success:function(data){
                    if(!data.success){
                         result = false;
                         alert(data.msg);    
                    }else{
                        subject_item.remove();                     
                        result = true;
                    }
                }});
                    return result;
            }
        }]});
    }
});
/*URL 验证  */
function validateURL(textval) {
  var urlregex = new RegExp(/^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/gi); 
  return urlregex.test(textval);
}
/*发表资源输入框错误提示*/
$('#form_url').live('input paste',function(){
             $('#modal-dialog-url-warnmsg-wrapper').css('display','none');});
$('#form_title').live('input paste',function(){
             $('#modal-dialog-title-warnmsg-wrapper').css('display','none')});
$('.tag-editor > input').live('input paste',function(){
            $('#form-tag-err').css('display','none');});

function check_login(){
    var authenticated  = false;
    $.ajax({type:"POST",url:"/account/islogin/",data:"",async:false,
            success:function(data){
                if(data=="null"){
                    signup();
                    authenticated =  false;
                }else{
                    authenticated =  true;
                }
            }
   });
   return authenticated;
};

/*发布资源链接地址检查*/
$('#form_url').live('change',function(){
    var form_url = $.trim($(this).val());
    $.ajax({type:"GET",url:"/subject/ajax/",data:{'url':form_url},
        success:function(data){
            if(typeof data.title!="undefined"){
               $('.form-subject-tips').html($('<p class="grey">可能有人已经提交过该资源：</p><a href=/subject/'+data.id+">"+data.title+"</a>"));
            }else{
                $('.form-subject-tips').html('');
            }
        }
    });
});
/*对话框*/
var subject_dialog = null;
$(".button-share").bind('click',function(e){
        e.preventDefault();
		if(check_login()){
            /*资源发布dialog*/
         subject_dialog = $.Zebra_Dialog(
            /* 对话框元素*/
            '<div id="content">'+
            '    <div class="form-url-wrapper">'+
            '    <div style="position: relative; display: none;" id="modal-dialog-url-warnmsg-wrapper">'+
            '         <div class="modal-dialog-warnmsg modal-dialog-guide-warn-message zg-r5px"></div>'+
            '            <span class="modal-dialog-guide-title-msg"></span>'+
            '    </div>'+
            '    <input id="form_url" name="url" required="true" type="text" placeholder="输入资源链接" >'+
            '    <div class="form-subject-tips"></div>'+
            '    </div>'+
            '    <div class="form-title-wrapper">'+
            '    <div style="position: relative; display: none;" id="modal-dialog-title-warnmsg-wrapper">'+
            '        <div class="modal-dialog-warnmsg modal-dialog-guide-warn-message zg-r5px"></div>'+
            '        <span class="modal-dialog-guide-title-msg"></span>'+
            '    </div>'+
            '    <input id="form_title" name="title" required="true" type="text" placeholder="输入资源标题">'+
            '    </div>'+
            '    <div class="form-item">'+
            '        <label for="form_comments">想补充点什么吗？（可选）</label>'+
            '        <textarea id="form_comments" name="content"></textarea>'+
            '    </div>'+
            '    <div style="position: relative;margin:10px 0"> '+
            '	   <div class="form-item">'+
            '        		<label for="form_tag">标签</label><span id="form-tag-err" style="display: none;">至少添加一个标签</span>'+
            '				<input id="tagnames" name="tagnames" type="text" size="60" value="" tabindex="103" style="display: none;">'+
            '	    		<div class="actual-edit-overlay" style="width: 416px; height: 31px; opacity: 1; position: absolute;'+
            '               background-color: white; color: black; -webkit-text-fill-color: black; line-height: 28px; '+
            '               font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 13px; text-align: start; '+
            '               border: 1px solid rgb(153, 153, 153);" disabled="disabled">&nbsp;	'+
            '								至少选择一个标签，不能多于5个'+
            '				</div>'+
            '				<div class="tag-editor edit-field-overlayed" style="width: 416px; height: 31px;'+
            '               opacity: 0.4; z-index: 1; position: relative;">'+
            '					<span></span><input  type="text" tabindex="103"><span></span>'+
            '				</div>'+
            '				<span class="edit-field-overlay">at least one tag such as (sql-server django html), max 5 tags</span>'+
            '		</div>'+
            '	</div>'+
            '</div>',// end of dialog element
         {
             title:'发布资源',
             overlay_close:false,
             buttons:[
               {caption:"发布",callback:function(){
                       $('#form_url').tipsy({trigger:'manual',gravity:'s'});
                       var url = $.trim($('#form_url').val());
                       var title = $.trim($('#form_title').val());
                       var content = $.trim($('#form_comments').val());
                       content = replaceURLWithHTMLLinks(content);
                       var tags = $.trim($('#tagnames').val());          
                       var result = true; //返回给dialog的值
                         /*输入验证  */
                       if(!validateURL(url)){
                           //$('#form_url').tipsy("show",{live:true});
                           $('#modal-dialog-url-warnmsg-wrapper').css('display','block')
                                .find('.modal-dialog-warnmsg').html('链接地址的格式貌似不正确，正确的格式 如：http://www.catb.org/esr/faqs/hacker-howto.html');
                       }else if(title.length < 3 ){
                           $('#modal-dialog-title-warnmsg-wrapper').css('display','block')
                                .find('.modal-dialog-warnmsg').html('标题短了点吧');
                       }else if(content.length>500){
                           alert('内容太长了，最多不能超过500个字哦');
                       }
                       else if(tags.length < 1 ){
                           var input_tag = $.trim($('.tag-editor input').val());
                           if(input_tag != '')
                               $('#form-tag-err').html('你暂时还没法创建"'+input_tag+'"标签，更换标签试试');
                           $('#form-tag-err').css('display',''); 
                       }else{
                           $.ajax({
                                type:'POST',
                                url:'/subject/add_subject/',
                                data:{'url':url,'title':title,'content':content,'tags':tags},
                                async:false,
                                success:function(data){
                                   if(data.success==false){
                                        result = false;
                                        alert(data.errors);    
                                   }else{
                                        result = true;
                                        subject_dialog.close();
                                        window.location = "/subject/recent";    
                                   }
                           }});
                        return result;
                }}}
             ],//end of buttons
		     width:460,
             position:['center','top-43'],
             modal:true,
             onClose:function(caption){
             }
         });//end of zebra_dialog
    /*
    $(
        '<div class="form-share"><span>分享到</span>'+
        '<span class="form-share-icon sina"></span></div>'
     ).prependTo(".ZebraDialog_Buttons");
     */
}//end of chek_login
});//endof button-share click
/* 删除标签  */
$('.delete-tag').live('click',function(){
    var post_tag = $(this).closest('.post-tag');
    del_tag(post_tag);
});
/*投票subject  */
$(".up").click(function(){
    thiz = $(this);
    var subj_item = thiz.closest(".subject-item");
    var data_id = subj_item.attr("data-id");
    var r_url = "/subject/"+data_id+"/favour/"; 
    var pressed = thiz.hasClass('pressed');
    var vote_tag = subj_item.children('.subject-item-meta').find('span:first');
    var vote = vote_tag.html().match(/\d+/g);

    if (!pressed){
    $.ajax({
        type:"POST",
        url:r_url+"?method=vote_up",
        data:{"subject_id":data_id},
        dataType:'json',
        async:false,
        success:function(data){
            if (data){
                if (check_authentication(data)){
                    if(data.msg=='success'){
	                     thiz.css({'background':'#698ebf'});
	                     thiz.children("span").css({'color':'#e7f3f9'});
	                     thiz.addClass("pressed");
                         vote_tag.html((parseInt(vote)+1)+'票');
                         thiz.attr('original-title','取消赞');
                         thiz.tipsy({trigger:'manual'});
                         thiz.tipsy('show');
                         thiz.tipsy({trigger:'hover'});
                    }else{alert(data);}//fixme 
                }else{signup();}
            }
        }}); 
    }else{
        $.ajax({type:"POST",url:r_url+"?method=vote_down",data:{"subject_id":data_id},dataType:'json',async:false,
        success:function(data){
            if(data.msg=='success'){
                thiz.attr('style','');
	            thiz.removeClass("pressed");
                vote_tag.html((parseInt(vote)-1)+'票');
                thiz.attr('original-title','赞一个');
                thiz.tipsy({trigger:'manual'});
                thiz.tipsy('show');
                thiz.tipsy({trigger:'hover'});
            }else if(data.mgs='disfavoured') {
                alert('状态错误')
            }else if (data.not_authenticated){
                signup();
            }
        }}); 
    }
});
/* 标签搜索*/
$( ".tag-editor").find('input')
    .live('blur',function(){
        var str = $("#tagnames").val();
        var is_blank = typeof str=='undefined' || $.trim(str)=="";
        if((!($('.autocomplete-suggestions').is(':visible')) && is_blank)&&$(this).val()==""){
            $('.tag-editor').css({'opacity':0.5});
        }
        
        return false;
    }).live('focus',function(event){
        $('.tag-editor').css({'opacity': 1});  /*隐藏提示语  */
		$(this).autocomplete({ /* 自动补全标签  */
            positionElement:$('.tag-editor'),
			serviceUrl:'/subject/tag_search2/',
			onSelect:function(suggestion){
				$('.tag-editor').css({'opacity': 1});
				var tagnames = $("#tagnames").val();
				tagnames += suggestion.value+" ";
				$("#tagnames").val(tagnames);
				$('<span class="post-tag">'+suggestion.value+'<span class="delete-tag"></span></span>')
				    .appendTo($(".tag-editor span:first"));
				$(this).val("");
			    $( ".tag-editor").find('input')[0].focus();
		    }});
	}).live('keydown',function(e){ /* 删除最后一个tag*/
        if($(this).val()=="" && e.keyCode==8){
            var last_tag = $(this).siblings(":first").find(">span").last();/*获取第一个兄弟元素的最后一个子元素*/
            if(last_tag[0]!=undefined){  
                del_tag(last_tag);
        }}
});
/* 删除标签*/
function del_tag(post_tag){
    var re = /(.+)<span/i;
    var arr = re.exec(post_tag.html());
    var tagnames = $('#tagnames').val();
    tagnames = tagnames.replace(arr[1],'');
    $('#tagnames').val(tagnames);
    post_tag.remove();
};

/*微博分享*/
function weibo_share(content,link){
    if (!check_login())
        return
    var result = false;
    $.Zebra_Dialog('<div class="zg-section">'+
                   '分享到：<span class="zg-ico-sina" ></span> 新浪微博'+
                   '<div class="zg-form-text-input">'+
                   '<textarea style="width: 100%; white-space: pre; height: 66px; min-height: 66px;" id="zh-webshare-sina-pm"'+
                   'class="zu-seamless-input-origin-element"></textarea></div></div>',
        {
        title:"分享资源",
        overlay_close:false, 
        buttons:[
            {caption:"分享",callback:function(){
                    var word_count = 130-$('#zh-webshare-sina-pm').val().length+weibo_share_link.length;
                    if (word_count <0){
                        alert('微博最多140个字符吖');
                        return;
                    }
                    var cont = $('#zh-webshare-sina-pm').val();
                    $.ajax({
                        type:'POST',async:false,
                         url:'/account/weibo_post/',
                         data:{'content':cont},
                         success:function(data){
                            if(data.success){
                                result = true;
                            }else{
                                alert(data.error);
                            }
                         }//alert(data.errors);}
                    });
               return result;
            }}],
        width:420,
        postion:['center','top+50'],
        modal:true,
        onClose:function(caption){
            if(result){
                $.Zebra_Dialog('<div class="report-dialog-return">分享成功</div>',
                    {   'auto_close':1000,
                        'buttons':false,
                        'title':'分享',
                    });
            }}
}
    );   
    $('#zh-webshare-sina-pm').val(content+' '+link);
    $('.form-share').html('还可以输入'+(130-$('#zh-webshare-sina-pm').val().length+weibo_share_link.length)+'个字符');
};
/*微博统计字数*/
$('#zh-webshare-sina-pm').live('propertychange keyup input paste focus',function(){
    var word_count = 130-$(this).val().length+weibo_share_link.length;
    if (word_count>0)
        $('.form-share').html('还可以输入'+(130-$(this).val().length+weibo_share_link.length)+'个字符');
    else
        $('.form-share').html('有'+(-word_count)+'个字符溢出啦!!!');
});

/* 弹出评论按钮 */
$('.zm-comment-editable').live('click',function() {
    //focus
    var div = $(this)[0];
    div.onfocus = function() {
        window.setTimeout(function() {
            var sel, range;
            if (window.getSelection && document.createRange) {
                range = document.createRange();
                range.selectNodeContents(div);
                range.collapse(true);
                sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (document.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(div);
                range.collapse(true);
                range.select();
            }
    }, 1);};
    $(this)[0].focus();
    if($(this).html().indexOf('<p style')!=-1){
        $(this).empty();
    }
    $(this).next('.zm-command').css('display','block');
});

/*隐藏评论按钮 */
function hidden_comment_button(){
    $('.zm-command').css('display','none');
    $('.zm-comment-editable').html('<p style="color:#999">写下你的评论…</p>');
    $('<input>').appendTo('body').focus().remove(); //光标跳转到临时创建元素
}
$('.zm-command-cancel').live('click',function(){
    hidden_comment_button();
});
/*  修改主题*/
$("#subject_edit").bind('click',function(){
    var content = $(this).parent().prev().text() 
    content = replaceURLWithHTMLLinks(content)
    var data_id = $('.subject-item').attr('data-id'); 
    var url = "/subject/"+data_id+"/edit/";

    $.ajax({
        type:"POST",
        url:url,
        data:{'content':content},
        dataType:'json',
        success:function(data){
            if(data == 'not_authenticated'){
               signup(); 
            }else{
                window.location = '/subject/'+data_id;
            }
        }});
})
/* 添加评论:fixme(清除内容样式)*/
$('#add_comment').live('click',function(){
    var comment_id = null;
    if($(this).closest('.zm-item-comment').length>0){
        //回复评论
         comment_id = $(this).closest('.zm-item-comment').attr('data-id');
    }
    var thiz = $(this);
    var content = $(this).parent().prev().text();
    if($.trim(content)==''){
        alert('说两句吧！');
        return;
    }
    var data_id = $('.subject-item').attr('data-id'); 
    var url = "/subject/"+data_id+"/comment/add/";
    $.ajax({
        type:"POST",
        url:url,
        data:{'content':content,'comment_id':comment_id},
        dataType:'html',
        success:function(data){
            if(data == 'not_authenticated'){
               signup(); 
            }else{
                $(data).appendTo($('.zm-comment-list'));
                hidden_comment_button();
                if(comment_id!=null){
                   thiz.closest('.zm-comment-form').css('display','none'); 
                }
            }
        }});
});
/*赞评论*/
$('.zm-comment-ft > .like').live('click',function(){
   var thiz = $(this);
   var data_id = $(this).closest('.zm-item-comment').attr('data-id');
   var url = "/subject/comment/"+data_id+"/like/";
   $.ajax({
       type:"POST",
       url:url,
       data:{},
       dataType:'json',
       async:false,
       success:function(data){
           if(data.msg=='success'){
                like_num = thiz.siblings('.like-num').children('em');
                thiz.siblings('.like-num').removeClass('nil');
                like_num.html(parseInt(like_num.html())+1);
                thiz.html('已赞');
           }
       }});
});



/*回复评论*/
//TODO
$('.zm-comment-ft > .reply').live('click',function(){
var comment = '<div class="zm-comment-form zm-comment-form-editing">'+
              '		<div class="zm-comment-editable editable" aria-label="写下你的回复" g_editable="true" contenteditable="true"></div>'+
              '		<div class="zm-command zg-clear">'+
              '		<a href="javascript:;" class="zm-comment-submit zg-right zg-btn-blue">评论</a>'+
              '		<a href="javascript:;" class="zm-comment-close zm-command-cancel">取消</a>'+
              '		</div>'+
              '</div>';
    if($(this).parent().next('.zm-comment-form').length<=0){
        $(this).closest('.zm-comment-ft').after(comment);
    //}
    //if (comment!=''){
    //    $(this).closest('.zm-comment-ft').after(comment);
    //    comment = '';
    }else{
        $(this).closest('.zm-comment-ft').next().toggle();
    }
});

/*删除评论*/
$('.zm-comment-ft >.delete').live('click',function(){
   var thiz = $(this);
   var data_id = $(this).closest('.zm-item-comment').attr('data-id');
   var url = "/subject/comment/"+data_id+"/delete/";
   $.Zebra_Dialog('<div id="content"><p style="font-size:14px">一定要删除评论吗？</p></div>',
        {title:"删除评论",buttons:[{caption:"确定",callback:function(){
            $.ajax({type:"POST",url:url,data:{},dataType:'json',
                async:false,
                success:function(data){
                    if(data.success){
                        thiz.closest('.zm-item-comment').remove();
                    }else{
                         alert(data.msg);
                    }
                }});
                    return true;
                }
    }]});
});

/*举报评论*/
$('.zm-comment-ft >.report').live('click',function(){
    var comment_id = $(this).closest('.zm-item-comment').attr('data-id');
    report(comment_id,'comment');
});

/*搜索*/
$('.search-button').click(function(){
    var query = encodeURIComponent($('.question_box').val());
    if ($.trim(query)!=''){
        window.location = '/search?q='+query;
    }
});

/*修改个性签名*/
$('.show-intro .edit-button').click(function(){
    $('.userintro .intro_wrapper').css('display','block');
    $('.show-intro').css('display','none');});
$('#intro_cancel').click(function(){
     $('.show-intro').css('display','block');
     $('.userintro .intro_wrapper').css('display','none'); });

$('#intro_submit').click(function(){
   var content = $("#intro_content").val(); 
   $.ajax({
      type:"POST",
      url:$('#intro_url').val(),
      data:{'content':content},
      dataType:'json',
      success:function(data){
          if(data.msg=='success'){
               $('.show-intro span').text(content);
               $('.show-intro').css('display','block');
               $('.userintro .intro_wrapper').css('display','none'); 
               if($("#intro_content").val()!="")
                   $('.edit-button').html('修改');
                else
                   $('.edit-button').html('一句话介绍自己');

          }
      }});
});

$("advice").click(function(){

     $.Zebra_Dialog('<div class="report-dialog-content"><form><ul class="options clearfix"><li><label><input value="101" name="reason" type="radio"> 广告垃圾信息</label></li><li><label><input value="102" name="reason" type="radio"> 骚扰谩骂 </label></li><li><label><input value="103" name="reason" type="radio"> 政治敏感内容 </label></li><li><label><input value="104" name="reason" type="radio"> 病毒木马</label></li><li><label><input value="105" name="reason" type="radio"> 其他</label></li><li><a href="#" target="_blank">了解更多</a></li></ul></form><div class="report-dialog-desc"><p>举报说明（可选）：<p><input type="text" class="reason_desc" maxlength="100" placeholder="描述恶意行为"></div></div>',
   {
       title:'我要吐槽',
       overlay_close:false,
       type:'infomation',
       width:350,
       mode:true,
       buttons:[{caption:"确定",callback:function(){
           var reason = $('input[name=reason]:checked', 'form').val();
           if (typeof(reason)=='undefined'){
                 $('.form-share').html('<span style="color:#EE5151">您必须选择一项举报理由</span>');
            return
           }
           var report_desc = $('.reason_desc').val();
           var url = null;
           var data= {'id':id,'reason':reason,'reason_desc':report_desc};
           if(type=='subject'){
                data.type = 'subject'; 
           }else{
               data.type = 'comment';
           }
           $.ajax({type:'POST',url:'/report/',async:false,
                    data:data,
                    success:function(data){
                    if(!data.success){
                         alert(data.msg);    
                         result = false;
                    }else{
                        result = true;
                    }
                }});
            return result; 
       }}],
       onClose:function(caption){
            if(result){
                $.Zebra_Dialog('<div class="report-dialog-return">举报成功，我们会及时处理</div>',
                    {   'auto_close':2000,
                        'buttons':false,
                        'title':'举报',
                    });
            }}
   });

});


/*获取验证码*/
function getCaptcha(){
   $captcha = $('.captcha'); 
    $.getJSON('/account/captcha/',function(data){
        $captcha.attr('src', data.new_cptch_image);
    });
};
$('.captcha').click(getCaptcha);
$("#captcha_change").click(getCaptcha);
    
}); //endof first row


