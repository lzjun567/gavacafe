# -*- encoding=UTF-8 -*-
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext

'''分页
name:模版中的变量名
obj_list:替换name的数据对象
request:http request 对象
template_name:模版名称
**kargs:模版中其它变量和值
'''
def paginate(name,obj_list,request,template_name,**kargs):
    paginator = Paginator(obj_list,settings.PAGINATOR_PER_PAGE)
    page = request.GET.get('p')
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    kargs[name] = objs
    return render_to_response(template_name,kargs,context_instance=RequestContext(request))


