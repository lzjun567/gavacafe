#####2014-01-28
+ django升级到1.6,升级后发现两处不兼容的方法
+ django.crontrib.markup 取消了,自己写了过滤器
+ SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'#1.6 default is JSONSerializer
#####2013-09-30
+ 添加用户注册模块
+ 修复资源添加BUG
#####2013-09-14
+ 添加网站更新日志链接
#####2013-09-01
+ 修复对资源“赞”的BUG
#####2013-08-31
+ 新增功能：修改资源
#####2013-08-25
+ 新增功能：发布资源时可选内容中的超链接替换普通文本URL
#####2013-08-19
+ 新增标签搜索
+ 修复 forword_url  KeyError 错误
